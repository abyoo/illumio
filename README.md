## Illumio Coding Challenge: NAT

### Compiling and Running the Program
1. Compile using g++ nat.cpp -o nat.
2. Run using ./nat NAT FLOW where NAT and FLOW are the names of the NAT file and 
FLOW file respectively. I have provided a NAT file called nat_file.txt and a FLOW 
file called flow_file.txt.

### Testing My Solution
I believe this area to be the **weakest** since I did not have time within 90 
to extensively test my solution. I only used the example that was provided 
in the email that introduced this coding challenge. I retrieved the same results 
as the email. Because of my limited testing, I would have spent more time fleshing 
out error handling in edge cases.

For example. I needed to figure out more precisely what occurs when there are 
multiple private addresses that could match a particular address within the 
NAT file. For instance, *:8080 and 10.1.0.1:* would both be a match for 
10.1.0.1:8080, but these addresses may have different addresses for 
translation within the NAT file. I would have determined how this special case 
should be handled by researching more about NAT because this would have created 
a more convincing program (**improvement**). Some temporary options that I considered were to 
translate the address based on the first address that appears in the NAT file 
or to throw an error.

In addition, another key error that was not handled in my program was whether each
line in the NAT file and FLOW file were valid. I will list some of the errors that I
would have handled given more time to write my coding challenge:

1. I would have checked whether each line contains one valid IP address for a FLOW file 
and two valid IP addresses in a NAT file (**improvement**). This means that each IP address has exactly three 
periods that separate four numbers ranging from 0 to 255.
2. I would have checked whether each line contains one port number for a FLOW file and two 
port numbers for a NAT file (**improvement**). This means that this part of the line are both numbers that range 
from 0 to 65535.
3. The NAT file is in the format <ip addr>:<port #>,<ip addr>:<port #> without any additional
spaces or punctuation (**improvement**).

### Coding Choices
There are several ways of approach this coding challenge. I considered two methods that trade 
off both time and space complexity.

The first method is to read a line in the FLOW file first and then read each line in the NAT 
file until the program encounters a matching address. A matching address for 10.0.1.0:8080 
would be 10.0.1.0:8080, *:8080, or 10.0.1.0:*. This would be costly in terms of time complexity 
because the program would be reading the NAT file multiple times. However, this would minimize 
space complexity because the contents of the NAT file is not saved into a data structure. 

The second method is to read the entire NAT file *once* and save the contents into a hash map. 
For each line in the FLOW file, the program will do an average O(1) lookup into the hash map 
instead of searching through the entire NAT file. However, we need to store the hash map, which 
will increase space complexity. However, there is another weakness to this approach. Earlier in the 
README, I mentioned that one way to handle a special case in which an address might translate to 
multiple different addresses was to translate the address based on the first address. However, this 
is not easy to achieve using a hash map because the data structure does not account for the order in 
which the elements were inserted. As a result, we can either signify the order within the key that 
would be inserted into the hash map or proceed with the first method (**possible improvement**).

Below is a list of how I handled certain errors in my program:
1. If a line in the NAT file does not contain a comma, there will be an error message and the program 
will skip the line.
2. If a line in the NAT file contains two of the same source addresses, then there will be an error 
message and the program will skip the line.
3. If a line in the FLOW file does not contain a colon, there will be error message and the program will
treat the IP address and port numbers as empty strings.
4. If the program cannot open the files in the arguments, there will be an error message and the program 
will abort.

Several error messages that were not considered are detailed in the previous section. I understand that 
my program is limited and the few error messages that I omitted are far from the number of edge cases 
possible in this coding challenge (**improvement**).

### Improvements
One improvement that I would have made to my program is that I could have made the program easier to read. 
The program is lacking comments in blocks that might be difficult to understand. The method names might not 
be the most intuitive given their purpose within the program. 

In addition, I separate methods for dealing with the NAT file and the FLOW file. This could have been reduced 
to a single method to reduce the number of lines of code within the program.

I have mentioned several improvements in the previous sections. I have labeled these improvements.
