#include <string>
#include <iostream>
#include <fstream>
#include <unordered_map>

using namespace std;

/* 
 * This method splits the source and target from the NAT file.
 */
void split_src_tgt(string line, unordered_map<string, string>& hash_map){
  char sep = line.find_first_of(',');
  if(sep == std::string::npos){
    cout << "Warning: invalid line found in file\n";
    return;
  }

  string src = line.substr(0, sep);
  string tgt = line.substr(sep+1);

  auto it = hash_map.find(src);
  if(it == hash_map.end()){
    hash_map[src] = tgt;
  }
  //skips the line if there is a duplicate
  else{
    cout << "Warning: duplicate input\n";
  }
}



/* 
 * This methods splits the IP number and port number
 * in a single line
 */
pair<string, string> split_ip_port(string line){
  char sep = line.find_first_of(':');

  if(sep == std::string::npos){
    cout << "Warning: invalid line found in file\n";
    return make_pair("", "");
  }

  string ip = line.substr(0, sep);
  string port = line.substr(sep+1);
  
  return make_pair(ip, port);
}



/*
 * This method reads line by line from the NAT file and creates
 * a data structure that makes searching for source and target
 * mapping easier
 */
unordered_map<string, string> read_from_nat(ifstream& file){
  unordered_map<string, string> result{};
  string line;

  while(getline(file, line)){
    split_src_tgt(line, result);
  }

  file.close();
  return result;
}



/*
 * This method reads from the FLOW file and uses the mapping
 * to create the output file.
 */
void create_out(unordered_map<string, string> hash_map, ifstream& flow, ofstream& out){
  string line;
  
  while(getline(flow, line)){
    pair<string, string> ip_port = split_ip_port(line);
    
    auto it = hash_map.find(line);
    if(it != hash_map.end()){
      out << line << "->" << hash_map[line] << "\n";
      continue;
    }

    auto it2 = hash_map.find("*:"+ip_port.second);
    if(it2 != hash_map.end()){
      out << line << "->" << hash_map["*:"+ip_port.second] << "\n";
      continue;
    }

    auto it3 = hash_map.find(ip_port.first + ":*");
    if(it3 != hash_map.end()){
      out << line << "->" << hash_map[ip_port.first+":*"] << "\n";
      continue;
    }

    out << "No nat match for " << line << "\n";
  }

  flow.close();
  out.close();
}



int main(int argc, char** argv){
  if(argc != 3){
    cout << "Needs two files\n";
    return 0;
  }

  string nat_name = argv[1];
  string flow_name = argv[2];

  ifstream nat_file(nat_name);
  ifstream flow_file(flow_name);
  
  ofstream out_file("out.txt");

  if(!nat_file.is_open()){
    cout << "Could not open NAT file\n";
    return 0;
  }
  if(!flow_file.is_open()){
    cout << "Could not open FLOW file\n";
    return 0;
  }

  unordered_map<string, string> hash_map = read_from_nat(nat_file);
  create_out(hash_map, flow_file, out_file);
  return 0;
}


